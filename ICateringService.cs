﻿using ExternalServices.Dominio;
using ExternalServices.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICateringService" in both code and config file together.
    [ServiceContract]
    public interface ICateringService
    {
        [FaultContract(typeof(ReservaCateringException))]
        [OperationContract]
        string SolicitarReserva(ReservaCatering reservaCatering);

        [FaultContract(typeof(ReservaCateringException))]
        [OperationContract]
        Boolean EliminarReserva(string CodigoReserva);



    }
}
