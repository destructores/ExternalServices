﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ExternalServices.Dominio
{
    [DataContract]
    public class ReservaCatering
    {
        [DataMember]		
        public string NombreServicio { get; set; }

        [DataMember]
        public string CodigoReserva { get; set; }
        
        [DataMember]
        public string DescripcionServicio { get; set; }

        [DataMember]
        public int Cantidad { get; set; }

        [DataMember]
        public string FechaPedido { get; set; }
    }
}