﻿using ExternalServices.Dominio;
using ExternalServices.Errores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ExternalServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICateringServiceRest" in both code and config file together.
    [ServiceContract]
    public interface ICateringServiceRest
    {
        [FaultContract(typeof(ReservaCateringException))]
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ServiciosCatering", ResponseFormat = WebMessageFormat.Json)]
        string SolicitarReserva(ReservaCatering reservaCatering);

        [FaultContract(typeof(ReservaCateringException))]
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "ServiciosCatering/{CodigoReserva}", ResponseFormat = WebMessageFormat.Json)]
        Boolean EliminarReserva(string CodigoReserva);
    }
}
