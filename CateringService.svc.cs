﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ExternalServices.Dominio;
using ExternalServices.Persistencia;

namespace ExternalServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CateringService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CateringService.svc or CateringService.svc.cs at the Solution Explorer and start debugging.
    public class CateringService : ICateringService
    {

        RerservaCateringDAO reservaCateringDAO = new RerservaCateringDAO();

        public bool EliminarReserva(string CodigoReserva)
        {
            reservaCateringDAO.Eliminar(CodigoReserva);
            return true;
            
        }

        public string SolicitarReserva(ReservaCatering reservaCatering)
        {
            return reservaCateringDAO.SolicitarReserva(reservaCatering);
        }
    }
}
